var q = require('q');
var azure = require('azure');
var mime = require('mime');

/**
 * @typedef {Object} BlobManagerOptions
 * @property {Number} identifierLength - The randomly generated ID length. (default: 6)
 */

/**
 * @param {(String|BlobManagerOptions)} [containerNameOrOptions] - The container name or BlobManager options.
 * @param {BlobManagerOptions} [options] - The BlobManager options.
 * @constructor
 * @example
 * var blobManager = new BlobManager('CONTAINER_NAME');
 * blobManager.createService('AZURE_STORAGE_ACCOUNT', 'AZURE_STORAGE_ACCESS_KEY');
 */
var BlobManager = function (containerNameOrOptions, options) {
    var containerName;
    if (typeof containerNameOrOptions === 'string') {
        containerName = containerNameOrOptions;
    } else if (typeof containerNameOrOptions === 'object') {
        options = containerNameOrOptions;
    }

    /**
     * @type {Array}
     * @private
     */
    this._delayedActions = [];

    /**
     * @type {String}
     * @private
     */
    this._containerName = null;

    /**
     * @type {BlobManagerOptions}
     * @private
     */
    this._options = options || {};

    containerName && this.setContainerName(containerName);
};

/**
 * @param {Function} callback
 * @private
 */
BlobManager.prototype._ensureBlobServiceExists = function (callback) {
    process.nextTick(function () {
        if (this._containerName && this._blobService instanceof azure.BlobService) {
            callback();
        } else {
            this._delayedActions.push(callback);
        }
    }.bind(this));
};

/**
 * @returns {String}
 * @private
 */
BlobManager.prototype._generateRandomId = function () {
    var chars = 'bcdfghjklmnpqrstvwxyz0123456789';
    var result = '';
    var index;

    for (index = this._options.identifierLength || 6; index > 0; index--) {
        result += chars.charAt(Math.floor(Math.random() * chars.length));
    }

    return result;
};

/**
 * @param {String} containerName - The container name.
 * @since 0.0.4
 */
BlobManager.prototype.setContainerName = function (containerName) {
    this._containerName = containerName;

    if (this._blobService instanceof azure.BlobService) {
        this._blobService.createContainerIfNotExists(this._containerName, function () {
            this._delayedActions.forEach(function (func, index) {
                this._delayedActions.splice(index, 1);

                func();
            }.bind(this));
        }.bind(this));
    }
};

/**
 * @param {String} storageAccount - The storage account.
 * @param {String} storageAccessKey - The storage access key.
 */
BlobManager.prototype.createService = function (storageAccount, storageAccessKey) {
    this._blobService = azure.createBlobService(storageAccount, storageAccessKey);
    this._containerName && this.setContainerName(this._containerName);
};

/**
 * @typedef {Function} BlobManagerSaveHandlerCallback
 * @param {String} id
 * @param {Object} options
 * @param {Function} callback
 * @private
 */

/**
 * @typedef {Function} BlobManagerSaveHandlerFunc
 * @param {String} id
 * @param {Object} options
 * @param {BlobManagerSaveHandlerCallback} callback
 * @private
 */

/**
 * @param {BlobManagerSaveHandlerFunc} handler
 * @param {String|Object} [idOrMetadata]
 * @param {Object} [metadata]
 * @private
 */
BlobManager.prototype._save = function (handler, idOrMetadata, metadata) {
    var id = idOrMetadata;
    if (typeof id === 'object') {
        metadata = id;
        id = null;
    }

    metadata = metadata || {};
    metadata.updated = Date.now();

    if (!metadata.created) {
        metadata.created = metadata.updated;
    }

    var options = {
        metadata: metadata
    };

    var attempt;
    var handle = function () {
        if (!id) {
            id = this._generateRandomId();

            !attempt && (attempt = 1);

            // Make sure resource does not exist
            options.accessConditions = options.accessConditions || {};
            options.accessConditions[azure.Constants.HeaderConstants.IF_NONE_MATCH] = '*';
        }

        handler(id, options, function (err, blob) {
            if (err) {
                if (err.code === azure.Constants.BlobErrorCodeStrings.BLOB_ALREADY_EXISTS && attempt && attempt++ < 50) {
                    return id = null, handle();
                }

                return deferred.reject(err);
            }

            blob = blob || {};
            if (typeof blob === 'string') {
                blob = { blob: blob };
            }

            deferred.resolve({
                id: blob.blob || id,
                metadata: blob.metadata || options.metadata
            });
        });
    }.bind(this);

    var deferred = q.defer();

    this._ensureBlobServiceExists(handle);

    return deferred.promise;
};

/**
 * @param {(String|Object)} data - The blob content.
 * @param {(String|Object)} [idOrMetadata] - The blob ID or metadata.
 * @param {Object} [metadata] - The blob metadata.
 * @returns {q.promise}
 * @example
 * blobManager.save({ foo: 'bar' })
 *     .then(function (blob) {
 *         // `blob` is an object with `id` and `metadata` properties
 *         console.log('Blob',
 *             '\n\tID:', blob.id,
 *             '\n\tMetadata:', blob.metadata);
 *     });
 * @example <caption>You can add metadata to the blob</caption>
 * blobManager.save({ foo: 'bar' }, { version: '1.0.0' });
 * @example <caption>You also can update content of the existing blob</caption>
 * blobManager.save({ foo: 'bar', baz: 'qux' }, 'BLOB_ID', { version: '2.0.0' });
 */
BlobManager.prototype.save = function (data, idOrMetadata, metadata) {
    if (typeof data === 'object') {
        data = JSON.stringify(data);
    }

    var handler = function (id, options, callback) {
        this._blobService.createBlockBlobFromText(this._containerName, id, data, options, callback);
    }.bind(this);

    return this._save(handler, idOrMetadata, metadata);
};

/**
 * @param {String} id - The blob ID.
 * @param {BlobManagerBlobAndRequestOptions} [options] - The blob and request options.
 * @returns {q.promise}
 * @example
 * blobManager.find('BLOB_ID')
 *     .then(function (blob) {
 *         // `blob` is an object with `data` and `metadata` properties
 *         console.log('Blob',
 *             '\n\tData:', blob.data,
 *             '\n\tMetadata:', blob.metadata);
 *     });
 */
BlobManager.prototype.find = function (id, options) {
    options = options || {};

    var handle = function () {
        this._blobService.getBlobToText(this._containerName, id, options, function (err, data, _, res) {
            if (err) {
                return deferred.reject(err);
            }

            try {
                data = JSON.parse(data);
            } catch (_) {}

            res = res || {};

            var metadata = {};
            var headers = res.headers;
            var header;

            if (headers) {
                for (header in headers) {
                    if (headers.hasOwnProperty(header) && header.indexOf('x-ms-meta-') !== -1) {
                        metadata[header.substr(10)] = headers[header];
                    }
                }
            }

            deferred.resolve({
                data: data,
                metadata: metadata
            });
        });
    }.bind(this);

    var deferred = q.defer();

    this._ensureBlobServiceExists(handle);

    return deferred.promise;
};

/**
 * @param {BlobManagerListingAndRequestOptions} [options] - The listing and request options.
 * @returns {q.promise}
 * @example
 * blobManager.findAll()
 *     .then(function (blobs) {
 *         // `blobs` is an array with `blob` objects
 *         console.log('Blobs');
 *         blobs.forEach(function (blob) {
 *             // `blob` is an object with `id` and `metadata` properties
 *             console.log('\tBlob',
 *                 '\n\t\tID:', blob.id,
 *                 '\n\t\tMetadata:', blob.metadata);
 *         });
 *     });
 */
BlobManager.prototype.findAll = function (options) {
    options = options || {};
    options.include = 'metadata';

    var list = [];
    var handle = function () {
        this._blobService.listBlobs(this._containerName, options, function (err, blobs, res) {
            if (err) {
                return deferred.reject(err);
            }
            blobs.forEach(function (blob) {
                var item = {
                    id: blob.name,
                    metadata: blob.metadata
                };

                blob.snapshot && (item.timestamp = blob.snapshot);

                list.push(item);
            });

            options.marker = res.nextMarker || null;
            if (options.marker) {
                return handle();
            }

            deferred.resolve(list);
        });
    }.bind(this);

    var deferred = q.defer();

    this._ensureBlobServiceExists(handle);

    return deferred.promise;
};

/**
 * @param {BlobManagerListingAndRequestOptions} [options] - The listing and request options.
 * @returns {q.promise}
 * @example
 * blobManager.findSnapshots()
 *    .then(function (snapshots) {
 *        // `snapshots` is an array with `snapshot` objects
 *        console.log('Snapshots');
 *        snapshots.forEach(function (snapshot) {
 *            // `snapshot` is an object with `id`, `metadata`,
 *            // and `timestamp` properties
 *            console.log('\tSnapshot',
 *                '\n\t\tID:', snapshot.id,
 *                '\n\t\tMetadata:', snapshot.metadata,
 *                '\n\t\tTimestamp:', snapshot.timestamp);
 *        });
 *    });
 */
BlobManager.prototype.findSnapshots = function (options) {
    options = options || {};
    options.include = { include: 'metadata,snapshots' };

    var list = [];
    var snapshots = {};
    var handle = function () {
        this.findAll(options)
            .then(function (blobs) {
                blobs.forEach(function (blob) {
                    if (!blob.snapshot || (snapshots[blob.id] && new Date(snapshots[blob.id].snapshot) >= new Date(blob.snapshot))) {
                        return;
                    }

                    snapshots[blob.id] = blob;
                });

                Object.keys(snapshots).forEach(function (id) {
                    list.push(snapshots[id]);
                });

                deferred.resolve(list);
            })
            .fail(deferred.reject);
    }.bind(this);

    var deferred = q.defer();

    process.nextTick(handle);

    return deferred.promise;
};

/**
 * @param {String} id - The blob ID.
 * @returns {q.promise}
 * @example
 * blobManager.remove('BLOB_ID')
 *     .then(function () {
 *         // ...
 *     });
 */
BlobManager.prototype.remove = function (id) {
    var options = {};
    var deferred = q.defer();

    this._ensureBlobServiceExists(function () {
        this._blobService.deleteBlob(this._containerName, id, options, function (err, successful) {
            if (err) {
                deferred.reject(err);
            } else if (!successful) {
                deferred.reject(new Error('Blob not deleted: ' + id));
            } else {
                deferred.resolve(true);
            }
        });
    }.bind(this));

    return deferred.promise;
};

/**
 * @param {String} id - The blob ID.
 * @returns {q.promise}
 * @example
 * blobManager.backup('BLOB_ID')
 *     .then(function (snapshot) {
 *         // `snapshot` is an object with `id` and `timestamp` properties
 *         console.log('Snapshot',
 *             '\n\tID:', snapshot.id,
 *             '\n\tTimestamp:', snapshot.timestamp);
 *     });
 */
BlobManager.prototype.backup = function (id) {
    var options = { include: 'metadata' };
    var deferred = q.defer();

    this._ensureBlobServiceExists(function () {
        this._blobService.createBlobSnapshot(this._containerName, id, options, function (err, snapshot) {
            if (err) {
                return deferred.reject(err);
            }
            deferred.resolve({
                id: id,
                timestamp: snapshot
            });
        });
    }.bind(this));

    return deferred.promise;
};

/**
 * @param {String} id - The blob ID.
 * @param {String} timestamp - The snapshot timestamp.
 * @returns {q.promise}
 * @example
 * blobManager.restore('BLOB_ID', 'TIMESTAMP')
 *     .then(function () {
 *         // ...
 *     });
 */
BlobManager.prototype.restore = function (id, timestamp) {
    var options = { snapshotId: timestamp };
    var deferred = q.defer();

    process.nextTick(function () {
        this.find(id, options)
            .then(function (blob) {
                this.save(blob.data, id, blob.metadata)
                    .then(function () {
                        this.remove(id, options)
                            .then(deferred.resolve)
                            .fail(deferred.reject);
                    }.bind(this))
                    .fail(deferred.reject);
            }.bind(this))
            .fail(deferred.reject);
    }.bind(this));

    return deferred.promise;
};

/**
 * @param {Stream} readStream - The read stream.
 * @param {String|Object} [idOrMetadata] - The blob ID or metadata.
 * @param {Object} [metadata] - The blob meatadata.
 * @returns {q.promise}
 * @example
 * blobManager.upload(fs.createReadStream('/path/to/file'))
 *     .then(function (blob) {
 *         // `blob` is an object with `id` and `metadata` properties
 *         console.log('Blob',
 *             '\n\tID:', blob.id,
 *             '\n\tMetadata:', blob.metadata);
 *     });
 * @example <caption>You can add metadata to the blob</caption>
 * blobManager.upload(fs.createReadStream('/path/to/file'), { version: '1.0.0' });
 * @example <caption>You also can update content of the existing blob</caption>
 * blobManager.upload(fs.createReadStream('/path/to/file'), 'BLOB_ID', { version: '2.0.0' });
 */
BlobManager.prototype.upload = function (readStream, idOrMetadata, metadata) {
    var handler = function (id, options, callback) {
        options.metadata.filename = options.metadata.filename || Date.now() + '.png';
        if (options.metadata.filename.indexOf('/') !== -1) {
            options.metadata.filename = options.metadata.filename.substr(1 + options.metadata.filename.lastIndexOf('/'));
        }
        options.metadata.filename = options.metadata.filename.replace(/[^\u0000-\u0080]+/g, '');
        options.contentType = options.contentTypeHeader = mime.lookup(options.metadata.filename);

        this._blobService.createBlockBlobFromStream(this._containerName, id, readStream, 32 * 1024 * 1024, options, callback);
    }.bind(this);

    return this._save(handler, idOrMetadata, metadata);
};

/**
 * @param {String} id - The blob ID.
 * @param {Stream} writeStream - The write stream.
 * @returns {q.promise}
 * @example
 * blobManager.download('BLOB_ID', fs.createWriteStream('/path/to/file'))
 *     .then(function (blob) {
 *         // `blob` is an object with `id` and `metadata` properties
 *         console.log('Blob',
 *             '\n\tID:' blob.id,
 *             '\n\tMetadata:', blob.metadata);
 *     });
 */
BlobManager.prototype.download = function (id, writeStream) {
    var deferred = q.defer();

    this._ensureBlobServiceExists(function () {
        var readStream = this._blobService.getBlob(this._containerName, id, function (err, blob) {
            if (err) {
                return deferred.reject(err);
            }

            blob.metadata.lastOpened = Date.now();

            deferred.resolve({
                id: blob.blob,
                metadata: blob.metadata
            });

            this.setMetadata(blob.blob, blob.metadata);
        }.bind(this));

        readStream.once('response', function (res, req) {
            writeStream.on('close', function() {
                req.abort();
            });

            if (res.statusCode !== 200) {
                var err = new Error('BlobNotFound');
                err.statusCode = 404;

                return deferred.reject(err);
            }

            var headers = {
                'cache-control': 'public, max-age=31536000'
            };

            [
                'content-type',
                'content-length',
                'content-md5',
                'etag'
            ].forEach(function (key) {
                var value = res.headers[key];
                if (value) {
                    writeStream.setHeader(key, value)
                }
            });

            readStream.pipe(writeStream);
        });

        readStream.once('error', function (err) {
            console.log('Error:', err.message);
        });
    }.bind(this));

    return deferred.promise;
};

/**
 * @param {String} id - The blob ID.
 * @param {Object} metadata - The blob metadata.
 * @returns {q.promise}
 * @example
 * blobManager.setMetadata('BLOB_ID', { version: '2.0.0' })
 *     .then(function () {
 *         // ...
 *     });
 */
BlobManager.prototype.setMetadata = function (id, metadata) {
    var deferred = q.defer();

    this._ensureBlobServiceExists(function () {
        this._blobService.setBlobMetadata(this._containerName, id, metadata, {}, function (err) {
            if (err) {
                return deferred.reject(err);
            }

            deferred.resolve(true);
        });
    }.bind(this));

    return deferred.promise;
};

/**
 * @param {String} id - The blob ID.
 * @returns {q.promise}
 * @example
 * blobManager.getMetadata('BLOB_ID')
 *     .then(function (metadata) {
 *         // `metadata` is an object with metadata
 *         console.log('Metadata:');
 *         Object.keys(metadata).forEach(function (key) {
 *             console.log(key, '=', metadata[key]);
 *         });
 *     });
 */
BlobManager.prototype.getMetadata = function (id) {
    var deferred = q.defer();

    this._ensureBlobServiceExists(function () {
        this._blobService.getBlobMetadata(this._containerName, id, {}, function (err, blob) {
            if (err) {
                return deferred.reject(err);
            }

            deferred.resolve(blob.metadata);
        });
    }.bind(this));

    return deferred.promise;
};

/**
 * @type {BlobManager}
 */
module.exports = BlobManager;

/**
 * @typedef {Object} BlobManagerListingAndRequestOptions
 * @property {String} prefix - The blob name prefix.
 * @property {String} delimiter - Delimiter, i.e. '/', for specifying folder hierarchy.
 * @property {Number} maxresults - Specifies the maximum number of blobs to return per call to Azure ServiceClient. This does NOT affect list size returned by this function. (maximum: 5000)
 * @property {String} marker - String value that identifies the portion of the list to be returned with the next list operation.
 * @property {String} include - Specifies that the response should include one or more of the following subsets: '', 'metadata', 'snapshots', 'uncommittedblobs'). Multiple values can be added separated with a comma (,)
 * @property {Number} timeoutIntervalInMs - The timeout interval, in milliseconds, to use for the request.
 */

/**
 * @typedef {Object} BlobManagerBlobAndRequestOptions
 * @property {String} snapshotId - The snapshot ID.
 * @property {String} leaseId - The lease ID.
 * @property {String} blobType - The type of blob to create: block blob or page blob.
 * @property {String} contentType - The MIME content type of the blob. The default type is application/octet-stream.
 * @property {String} contentEncoding - The content encodings that have been applied to the blob.
 * @property {String} contentLanguage - The natural languages used by this resource.
 * @property {String} contentMD5 - The MD5 hash of the blob content.
 * @property {String} cacheControl - The Blob service stores this value but does not use or modify it.
 * @property {String} rangeStart - Return only the bytes of the blob in the specified range.
 * @property {String} rangeEnd - Return only the bytes of the blob in the specified range.
 * @property {String} contentTypeHeader - The blob’s content type. (x-ms-blob-content-type)
 * @property {String} contentEncodingHeader - The blob’s content encoding. (x-ms-blob-content-encoding)
 * @property {String} contentLanguageHeader - The blob's content language. (x-ms-blob-content-language)
 * @property {String} contentMD5Header - The blob’s MD5 hash. (x-ms-blob-content-md5)
 * @property {String} rangeStartHeader - Return only the bytes of the blob in the specified range. If both Range and x-ms-range are specified, the service uses the value of x-ms-range.
 * @property {String} rangeEndHeader - Return only the bytes of the blob in the specified range. If both Range and x-ms-range are specified, the service uses the value of x-ms-range.
 * @property {String} rangeGetContentMd5 - When this header is set to true and specified together with the Range header, the service returns the MD5 hash for the range, as long as the range is less than or equal to 4 MB in size.
 * @property {String} cacheControlHeader - The blob's cache control. (x-ms-blob-cache-control)
 * @property {Object} accessConditions - The access conditions. See http://msdn.microsoft.com/en-us/library/dd179371.aspx for more information.
 * @property {Number} timeoutIntervalInMs - The timeout interval, in milliseconds, to use for the request.
 */
